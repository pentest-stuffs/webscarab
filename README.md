# WebScarab 20120422.001828

WebScarab is a framework for analysing applications that communicate using the
HTTP and HTTPS protocols. It is written in Java, and is thus portable to many
platforms. WebScarab has several modes of operation, implemented by a number of
plugins.

In its most common usage, WebScarab operates as an intercepting proxy, allowing
the operator to review and modify requests created by the browser before they
are sent to the server, and to review and modify responses returned from the
server before they are received by the browser.

WebScarab is able to intercept both HTTP and HTTPS communication. The operator
can also review the conversations (requests and responses) that have passed
through WebScarab.

Homepage: http://dawes.za.net/rogan/webscarab/#current

WebScarab was tested in this linux distributions:
* Debian Stretch
* Linux Mint

This version of WebScarab was repacked from the Kali Linux distribution.

## INSTALLATION

### Dependencies:
WebScarab is a java tool, which need for a correct running a few another
packages.

If you will install this by the `dpkg -i`, then make sure, that you have
installed all the depending packages:

```
apt-get install default-jre openjdk-8-jre
```

### Install from source:
Unpack the source package and run command (like root):

```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build Debian package
Run command (like root):
```
make build-deb
```

### Install from *.deb package:
```
dpkg -i webscarab*.deb
```
